import { userRouter } from './user/users.router';
import { Server } from './server/server';

const server = new Server();

server.bootstrap([userRouter]).then(server => {
    const server_info = server.aplication.address()
    console.log(`Server is listenning on ${server_info.address}:${server_info.port}`)
}).catch((err) => {
    console.log(`server failed to start`)
    console.error(err)
    process.exit(1)
})