import { User } from './users.model';
import { Router } from '../common/router'
import * as restify from 'restify'

class UsersRouter extends Router {
    constructor(){
        super()
        this.on('beforeRender', document  => document.password = undefined)
    }
    applyRoutes(application: restify.Server) {
        application.get('/users', (req, resp, next) => {
            User.find().then(this.render(resp,next))
        })

        application.get('/users/:id', (req, resp, next) => {
            User.findById(req.params.id).then(this.render(resp,next))
        })

        application.post('/users', (req, resp, next) => {
            let user = new User(req.body)
            user.save().then(this.render(resp,next))
        })

        application.put('/users/:id', (req, resp, next) => {
            const options = { overwrite: true } // sobrescrever todo o objeto
            User.update({ _id: req.params.id }, req.body, options) // filtra id, update com body, sobrescreve obj todo
                .exec() //para rodar a query
                .then(result => { 
                    if (result.n) return User.findById(req.params.id) // se teve linhas afetas retornar
                    else resp.send(404) // senao enviar 404
                })
                .then(this.render(resp,next))
        })

        application.patch('/users/:id', (req, resp, next) => {
            const options = { new : true } // para a query retornar o obj novo
            User.findByIdAndUpdate(req.params.id, req.body,options) // filtra id, update com body, options para return
                .then(this.render(resp,next))
        })

        application.del('/users/:id',(req, resp, next) => {
            User.remove({_id : req.params.id}) // remove no mongo, by filter id
                .exec()
                .then( (queryResult : any) => {
                    if (queryResult.result.n) resp.send(204) // se sucesso informa 204 
                    else resp.send(404) // se erro informa 404 
                    return next()
                })
        })
    }
}

export const userRouter = new UsersRouter()