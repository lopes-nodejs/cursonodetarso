import { mergePatchBody } from './merge-patch.parser';
import * as restify from 'restify'
import * as mongoose from 'mongoose'
import { environment } from '../common/environment'
import { Router } from '../common/router'
export class Server {

    aplication: restify.Server

    initializeDB(): mongoose.MongooseThenable {
        (<any>mongoose).Promise = global.Promise
        return mongoose.connect(environment.db.url, {
            useMongoClient: true
        })
    }

    initRoutes(routers: Router[]): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this.aplication = restify.createServer({
                    name: 'meat-api',
                    version: '1.0.0'
                })

                this.aplication.use(
                    restify.plugins.queryParser()
                )

                this.aplication.use(
                    restify.plugins.bodyParser()
                )

                this.aplication.use(
                    mergePatchBody
                )
                routers.map((router) => {
                    router.applyRoutes(this.aplication)
                })

                this.aplication.listen(environment.server.port, () => {
                    resolve(this.aplication)
                })

            } catch (error) {
                reject(error)
            }
        })
    }

    bootstrap(routers: Router[] = []): Promise<Server> {
        return this.initializeDB().then(() => this.initRoutes(routers).then(() => this))
    }

}